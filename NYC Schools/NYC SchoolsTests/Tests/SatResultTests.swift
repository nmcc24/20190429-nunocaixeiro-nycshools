//
//  SatResultTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SatResultTests: XCTestCase {
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testSatResultCoding() throws {
        let jsonData = MockResponseData.singleSatJson.data(using: .utf8)!
        let satResult = try! JSONDecoder().decode([SatResult].self, from: jsonData).first!
        XCTAssertEqual(satResult.studentsCount, 44)
        XCTAssertEqual(satResult.averageReadingScore, 407)
        XCTAssertEqual(satResult.averageMathScore, 386)
        XCTAssertEqual(satResult.averageWritingScore, 378)

        let newSatResult = SatResult(studentsCount: 1, averageReadingScore: 2, averageMathScore: 3, averageWritingScore: 4)
        let newJsonData = try! JSONEncoder().encode(newSatResult)
        let newDecodedSatResult = try! JSONDecoder().decode(SatResult.self, from: newJsonData)
        XCTAssertEqual(newSatResult, newDecodedSatResult)
    }
}
