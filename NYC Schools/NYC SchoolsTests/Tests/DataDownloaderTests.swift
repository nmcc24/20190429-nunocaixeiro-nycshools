//
//  DataDownloaderTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class DataDownloaderTests: XCTestCase {

    override func setUp() {
        super.setUp()

        continueAfterFailure = false
    }

    func testNonNilError() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        MockURLProtocol.requestHandler = { request in
            let nsError = NSError(domain: "MockURLProtocol.FailedRequest", code: 1, userInfo: [NSLocalizedDescriptionKey: "A failed request"])
            throw nsError
        }

        let expectation = XCTestExpectation(description: "Data task response")

        let url = URL(string: "https://data.cityofnewyork.us")!
        let dataTask = dataDownloader.dataTask(with: URLRequest(url: url)) { result in
            switch(result) {
            case .error(let error):
                let responseError = error as! ResponseError
                XCTAssertEqual(responseError, .undefined("A failed request"))
                expectation.fulfill()
            default: XCTFail()
            }
        }

        dataTask.resume()

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulResponseWithArrayOfDictionaries() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        let url = URL(string: "https://data.cityofnewyork.us")!
        let jsonData = MockResponseData.singleSatJson.data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, jsonData)
        }

        let expectation = XCTestExpectation(description: "Data task response")
        let dataTask = dataDownloader.dataTask(with: URLRequest(url: url)) { result in
            switch(result) {
            case .success(let result):
                XCTAssertEqual(result, jsonData)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        dataTask.resume()
        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulResponseWithDictionary() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        let url = URL(string: "https://data.cityofnewyork.us")!
        let jsonData = dictJson.data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, jsonData)
        }

        let expectation = XCTestExpectation(description: "Data task response")
        let dataTask = dataDownloader.dataTask(with: URLRequest(url: url)) { result in
            switch(result) {
            case .success(let result):
                XCTAssertEqual(result, jsonData)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        dataTask.resume()
        wait(for: [expectation], timeout: 1)
    }

    func testResponseWithErrorAndMessage() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        let url = URL(string: "https://data.cityofnewyork.us")!
        let jsonData = errorWithMessageJson.data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, jsonData)
        }

        let expectation = XCTestExpectation(description: "Data task response")
        let dataTask = dataDownloader.dataTask(with: URLRequest(url: url)) { result in
            switch(result) {
            case .error(let error):
                let responseError = error as! ResponseError
                XCTAssertEqual(responseError, .undefined("A test message"))
                expectation.fulfill()
            default: XCTFail()
            }
        }

        dataTask.resume()
        wait(for: [expectation], timeout: 1)
    }

    func testResponseWithErrorAndNoMessage() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        let url = URL(string: "https://data.cityofnewyork.us")!
        let jsonData = errorWithoutMessageJson.data(using: .utf8)!
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, jsonData)
        }

        let expectation = XCTestExpectation(description: "Data task response")
        let dataTask = dataDownloader.dataTask(with: URLRequest(url: url)) { result in
            switch(result) {
            case .error(let error):
                let responseError = error as! ResponseError
                XCTAssertEqual(responseError, .undefined(""))
                expectation.fulfill()
            default: XCTFail()
            }
        }

        dataTask.resume()
        wait(for: [expectation], timeout: 1)
    }
}

private extension DataDownloaderTests {
    var dictJson: String {
        return """
        {
          "dbn": "08X282",
          "num_of_sat_test_takers": "44",
          "sat_critical_reading_avg_score": "407",
          "sat_math_avg_score": "386",
          "sat_writing_avg_score": "378",
          "school_name": "WOMEN'S ACADEMY OF EXCELLENCE"
        }
        """
    }

    var errorWithMessageJson: String {
        return """
        {
          "error": "",
          "message": "A test message",
        }
        """
    }

    var errorWithoutMessageJson: String {
        return """
        {
          "error": ""
        }
        """
    }
}
