//
//  SchoolDetailsViewModelTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SchoolDetailsViewModelTests: XCTestCase {
    var school: School!
    var delegate: MockSchoolDetailsViewModelDelegate!
    var viewModel: SchoolDetailsViewModel!
    var service: MockSatResultsService!
    var resultsJson: String!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false

        let data = MockResponseData.schoolsJson.data(using: .utf8)!
        school = try! JSONDecoder().decode([School].self, from: data).first!

        delegate = MockSchoolDetailsViewModelDelegate()
        resultsJson = MockResponseData.singleSatJson
        service = MockSatResultsService(successJSON: resultsJson)
        viewModel = SchoolDetailsViewModel(school: school, service: service)
        viewModel.delegate = delegate
    }

    func testFailedFetch() {
        XCTAssertEqual(viewModel.title, school.name)
        XCTAssertEqual(viewModel.sections.count, 3)

        service.shouldFail = true
        viewModel.fetchSatResult()

        XCTAssertEqual(delegate.sectionsUpdatesCounter, 1)
        XCTAssertEqual(viewModel.sections.count, 4)
        validateSections(viewModel.sections, forSchool: school, satResults: nil)
    }

    func testSuccessfulFetch() {
        XCTAssertEqual(viewModel.title, school.name)
        XCTAssertEqual(viewModel.sections.count, 3)

        let data = resultsJson.data(using: .utf8)!
        let expectedSatResults = try! JSONDecoder().decode([SatResult].self, from: data).first!

        service.shouldFail = false
        viewModel.fetchSatResult()

        XCTAssertEqual(delegate.sectionsUpdatesCounter, 1)
        XCTAssertEqual(viewModel.sections.count, 4)
        validateSections(viewModel.sections, forSchool: school, satResults: expectedSatResults)
    }

    private func validateSections(_ sections: [SchoolDetailsViewModel.Section], forSchool school: School, satResults: SatResult?) {
        for section in sections {
            switch section {
            case .generalInfo(let items):
                XCTAssertEqual(items, [.text(school.overview),
                                       .detail(key: "Number of Students", value: String(school.studentsCount))])
            case .satResults(let items):
                if let results = satResults {
                    XCTAssertEqual(items, [.detail(key: "Reading Average Score", value: String(results.averageReadingScore)),
                                           .detail(key: "Math Average Score", value: String(results.averageMathScore)),
                                           .detail(key: "Writing Average Score", value: String(results.averageWritingScore))])
                } else {
                    XCTAssertEqual(items, [.text("No results available")])
                }
            case .address(let items):
                XCTAssertEqual(items, [.text(school.fullAddress)])
            case .contacts(let items):
                XCTAssertEqual(items, [.detail(key: "Website", value: String(school.website)),
                                       .detail(key: "Phone Number", value: String(school.phone))])
            }
        }
    }
}
