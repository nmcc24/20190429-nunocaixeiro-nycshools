//
//  ExtensionsTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class ExtensionsTests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testArraySafeSubscriptWithEmptyArray() {
        let array = [String]()
        XCTAssertNil(array[safe: 0])
    }

    func testArraySafeSubscriptWithInvalidIndex() {
        let array = ["N", "Y", "C"]
        XCTAssertNil(array[safe: -1])
        XCTAssertNil(array[safe: 3])
    }

    func testArraySafeSubscriptWithValidIndex() {
        let array = ["N", "Y", "C"]
        XCTAssertNotNil(array[safe: 0])
        XCTAssertNotNil(array[safe: 1])
        XCTAssertNotNil(array[safe: 2])
    }
}
