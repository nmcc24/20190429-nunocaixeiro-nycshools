//
//  SchoolsServiceTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SchoolsServiceTests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testEndpointDefaultConfiguration() {
        let service = SchoolsService()
        let components = service.endpointComponents

        guard let scheme = components.scheme,
            let host = components.host,
            let url = components.url else {
                XCTFail()
                return
        }

        XCTAssertEqual(scheme, "https")
        XCTAssertEqual(host, "data.cityofnewyork.us")
        XCTAssertEqual(components.path, "/resource/s3k6-pzi2.json")
        XCTAssertNil(components.query)
        XCTAssertEqual(url, URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"))
    }

    func testRetrievalWithInvalidURL() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "data.cityofnewyork.us"
        components.path = "resource/s3k6-pzi2.json" //invalid path by missing initial slash

        let service = SchoolsService(endpointComponents: components)
        let expectation = XCTestExpectation(description: "Service response")
        service.fetch { result in
            switch(result) {
            case .error(let error as ServiceError):
                XCTAssertEqual(error, ServiceError.invalidURL)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testRetrievalWithFailedRequest() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        MockURLProtocol.requestHandler = { request in
            self.validateHeaderFields(for: request)

            let nsError = NSError(domain: "MockURLProtocol.FailedRequest", code: 400, userInfo: [NSLocalizedDescriptionKey: "A failed request"])
            throw nsError
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SchoolsService(dataDownloader: dataDownloader)
        service.fetch { result in
            switch(result) {
            case .error:
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulRetrieval() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        MockURLProtocol.requestHandler = { request in
            self.validateHeaderFields(for: request)

            let jsonData = MockResponseData.schoolsJson.data(using: .utf8)!
            return (URLResponse(), jsonData)
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SchoolsService(dataDownloader: dataDownloader)
        service.fetch { result in
            switch(result) {
            case .success(let schools):
                let expectedJsonData = MockResponseData.schoolsJson.data(using: .utf8)!
                let expectedSchools = try! JSONDecoder().decode([School].self, from: expectedJsonData)
                XCTAssertEqual(schools, expectedSchools)

                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulRetrievalWithInvalidData() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)

        MockURLProtocol.requestHandler = { request in
            self.validateHeaderFields(for: request)

            let jsonData = MockResponseData.singleSatJson.data(using: .utf8)!
            return (URLResponse(), jsonData)
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SchoolsService(dataDownloader: dataDownloader)
        service.fetch { result in
            switch(result) {
            case .error(let error):
                let responseError = error as! ServiceError
                XCTAssertEqual(responseError, .failedJsonDecoding)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    private func validateHeaderFields(for request: URLRequest) {
        XCTAssertEqual(request.value(forHTTPHeaderField: "X-App-Token"), "cSXeLBmN3WkTgRKwdanapqYha")
        XCTAssertEqual(request.value(forHTTPHeaderField: "Accept"), "application/json")
    }
}
