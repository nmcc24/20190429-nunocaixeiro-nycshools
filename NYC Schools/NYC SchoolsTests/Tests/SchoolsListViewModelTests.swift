//
//  SchoolsListViewModelTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SchoolsListViewModelTests: XCTestCase {

    var delegate: MockSchoolsListViewModelDelegate!
    var viewModel: SchoolsListViewModel!
    var service: MockSchoolsService!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false

        delegate = MockSchoolsListViewModelDelegate()
        service = MockSchoolsService(successJSON: MockResponseData.schoolsJson)
        viewModel = SchoolsListViewModel(service: service)
        viewModel.delegate = delegate
    }

    func testFailedFetch() {
        service.shouldFail = true
        viewModel.fetchSchools()

        XCTAssertEqual(delegate.startLoadingStatusCounter, 1)
        XCTAssertEqual(delegate.stopLoadingStatusCounter, 1)
        XCTAssertEqual(delegate.schoolsSuccessfulFetchCounter, 0)
        XCTAssertEqual(delegate.schoolsFailedFetchCounter, 1)
        XCTAssertEqual(delegate.schoolsFailedFetchErrorMessages.count, 1)
        let message = delegate.schoolsFailedFetchErrorMessages[0]
        XCTAssertEqual(message, SchoolsListViewModel.Constants.errorMessage)

        XCTAssertTrue(viewModel.schools.isEmpty)
    }

    func testSuccessfulFetch() {
        let data = MockResponseData.schoolsJson.data(using: .utf8)!
        let expectedSchools = try! JSONDecoder().decode([School].self, from: data)

        service.shouldFail = false
        viewModel.fetchSchools()

        XCTAssertEqual(delegate.startLoadingStatusCounter, 1)
        XCTAssertEqual(delegate.stopLoadingStatusCounter, 1)
        XCTAssertEqual(delegate.schoolsSuccessfulFetchCounter, 1)
        XCTAssertEqual(delegate.schoolsFailedFetchCounter, 0)
        XCTAssertEqual(delegate.schoolsFailedFetchErrorMessages.count, 0)

        for (index, expectedSchool) in expectedSchools.enumerated() {
            XCTAssertEqual(expectedSchool, viewModel.schools[index])
        }
    }
}
