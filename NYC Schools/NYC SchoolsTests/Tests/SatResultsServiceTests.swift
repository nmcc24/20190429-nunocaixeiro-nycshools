//
//  SatResultsServiceTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SatResultsServiceTests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testEndpointDefaultConfiguration() {
        let service = SatResultsService()
        let components = service.endpointComponents

        guard let scheme = components.scheme,
            let host = components.host,
            let url = components.url else {
                XCTFail()
                return
        }

        XCTAssertEqual(scheme, "https")
        XCTAssertEqual(host, "data.cityofnewyork.us")
        XCTAssertEqual(components.path, "/resource/f9bf-2cp4.json")
        XCTAssertNil(components.query)
        XCTAssertEqual(url, URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"))
    }

    func testRetrievalWithInvalidURL() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "data.cityofnewyork.us"
        components.path = "resource/f9bf-2cp4.json" //invalid path by missing initial slash

        let service = SatResultsService(endpointComponents: components)
        let expectation = XCTestExpectation(description: "Service response")
        service.fetchResultForSchool(withId: "1") { result in
            switch(result) {
            case .error(let error as ServiceError):
                XCTAssertEqual(error, ServiceError.invalidURL)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testRetrievalWithFailedRequest() {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)
        let schoolId = "123"

        MockURLProtocol.requestHandler = { request in
            self.validateRequestContents(request, schoolId: schoolId)

            let nsError = NSError(domain: "MockURLProtocol.FailedRequest", code: 400, userInfo: [NSLocalizedDescriptionKey: "A failed request"])
            throw nsError
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SatResultsService(dataDownloader: dataDownloader)
        service.fetchResultForSchool(withId: schoolId) { result in
            switch(result) {
            case .error:
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulRetrieval() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)
        let schoolId = "123"

        MockURLProtocol.requestHandler = { request in
            self.validateRequestContents(request, schoolId: schoolId)

            let jsonData = MockResponseData.singleSatJson.data(using: .utf8)!
            return (URLResponse(), jsonData)
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SatResultsService(dataDownloader: dataDownloader)
        service.fetchResultForSchool(withId: schoolId) { result in
            switch(result) {
            case .success(let satResult):
                let expectedJsonData = MockResponseData.singleSatJson.data(using: .utf8)!
                let expectedSatResult = try! JSONDecoder().decode([SatResult].self, from: expectedJsonData).first!
                XCTAssertEqual(satResult, expectedSatResult)

                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulRetrievalWithEmptyData() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)
        let schoolId = "123"

        MockURLProtocol.requestHandler = { request in
            self.validateRequestContents(request, schoolId: schoolId)

            let jsonData = "[{}]".data(using: .utf8)!
            return (URLResponse(), jsonData)
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SatResultsService(dataDownloader: dataDownloader)
        service.fetchResultForSchool(withId: schoolId) { result in
            switch(result) {
            case .success(let satResult):
                XCTAssertNil(satResult)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    func testSuccessfulRetrievalWithInvalidData() throws {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]

        let urlSession = URLSession(configuration: configuration)
        let dataDownloader = DataDownloader(session: urlSession)
        let schoolId = "123"
        MockURLProtocol.requestHandler = { [unowned self] request in
            self.validateRequestContents(request, schoolId: schoolId)

            let jsonData = self.invalidSatResultJson.data(using: .utf8)!
            return (URLResponse(), jsonData)
        }

        let expectation = XCTestExpectation(description: "Service response")
        let service = SatResultsService(dataDownloader: dataDownloader)
        service.fetchResultForSchool(withId: schoolId) { result in
            switch(result) {
            case .success(let satResult):
                XCTAssertNil(satResult)
                expectation.fulfill()
            default: XCTFail()
            }
        }

        wait(for: [expectation], timeout: 1)
    }

    private func validateRequestContents(_ request: URLRequest, schoolId: String) {
        XCTAssertEqual(request.value(forHTTPHeaderField: "X-App-Token"), "cSXeLBmN3WkTgRKwdanapqYha")
        XCTAssertEqual(request.value(forHTTPHeaderField: "Accept"), "application/json")
        XCTAssertEqual(request.url?.query, "dbn=\(schoolId)")
    }
}

private extension SatResultsServiceTests {
    var invalidSatResultJson: String {
        return """
        [
          {
            "dbn": "02M399",
            "num_of_sat_test_takers": "s",
            "sat_critical_reading_avg_score": "s",
            "sat_math_avg_score": "s",
            "sat_writing_avg_score": "s",
            "school_name": "THE HIGH SCHOOL FOR LANGUAGE AND DIPLOMACY"
          }
        ]
        """
    }
}
