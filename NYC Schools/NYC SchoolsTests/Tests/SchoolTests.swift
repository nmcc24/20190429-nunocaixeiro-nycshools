//
//  SchoolTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class SchoolTests: XCTestCase {
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testSchoolCoding() throws {
        let jsonData = schoolJson.data(using: .utf8)!
        let school = try! JSONDecoder().decode(School.self, from: jsonData)
        XCTAssertEqual(school.id, "14K477")
        XCTAssertEqual(school.name, "School for Legal Studies")
        XCTAssertEqual(school.overview, "A small overview.")
        XCTAssertEqual(school.studentsCount, 509)
        XCTAssertEqual(school.website, "www.thesls.net")
        XCTAssertEqual(school.phone, "718-387-2800")
        XCTAssertEqual(school.primaryAddressLine, "850 Grand Street")
        XCTAssertEqual(school.city, "Brooklyn")
        XCTAssertEqual(school.stateCode, "NY")
        XCTAssertEqual(school.zipCode, "11211")
        XCTAssertEqual(school.fullAddress, "850 Grand Street\nBrooklyn\nNY 11211")

        let newSchool = School(id: "id", name: "name", overview: "overview",
                               studentsCount: 1, website: "website", phone: "phone",
                               primaryAddressLine: "address", city: "city",
                               stateCode: "state", zipCode: "zip")
        let newJsonData = try! JSONEncoder().encode(newSchool)
        let newDecodedSchool = try! JSONDecoder().decode(School.self, from: newJsonData)
        XCTAssertEqual(newSchool, newDecodedSchool)
    }
}

private extension SchoolTests {
    var schoolJson: String {
        return """
        {
          "city": "Brooklyn",
          "dbn": "14K477",
          "overview_paragraph": "A small overview.",
          "phone_number": "718-387-2800",
          "primary_address_line_1": "850 Grand Street",
          "school_name": "School for Legal Studies",
          "state_code": "NY",
          "total_students": "509",
          "website": "www.thesls.net",
          "zip": "11211"
        }
        """
    }
}
