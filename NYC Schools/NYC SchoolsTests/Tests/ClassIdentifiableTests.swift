//
//  ClassIdentifiableTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class ClassIdentifiableTests: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func testViewControllerSegueIdentifier() {
        let expectedIdentifier = "SchoolDetailsViewController"
        XCTAssertEqual(SchoolDetailsViewController.identifierFromClassName(), expectedIdentifier)
        XCTAssertEqual(SchoolDetailsViewController.segueIdentifier, "show" + expectedIdentifier)
    }
}
