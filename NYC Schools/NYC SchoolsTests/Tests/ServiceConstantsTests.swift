//
//  ServiceConstantsTests.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class ServiceConstantsTests: XCTestCase {

    func testUrlComponentsWithNormalisedPath() {
        let components = ServiceConstants.urlComponents(withPath: "s3k6-pzi2.json")
        XCTAssertEqual(components.url, URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"))
    }

    func testUrlComponentsWithNonNormalisedPath() {
        let components = ServiceConstants.urlComponents(withPath: "/s3k6-pzi2.json")
        XCTAssertEqual(components.url, URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"))
    }
}

