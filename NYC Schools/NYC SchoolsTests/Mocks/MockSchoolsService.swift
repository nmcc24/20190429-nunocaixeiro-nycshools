//
//  MockSchoolsService.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation
@testable import NYC_Schools

class MockSchoolsService: SchoolsServiceType {
    var endpointComponents = URLComponents()
    var shouldFail = false
    let successJSON: String

    init(successJSON: String) {
        self.successJSON = successJSON
    }

    func fetch(completion: @escaping (ServiceResult<[School]>) -> ()) {
        let result: ServiceResult<[School]>

        if shouldFail {
            result = .error(ResponseError.undefined("Test Response Error"))
        } else {
            let data = successJSON.data(using: .utf8)!
            let school = try! JSONDecoder().decode([School].self, from: data)
            result = .success(school)
        }

        completion(result)
    }
}
