//
//  MockSchoolDetailsViewModelDelegate.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class MockSchoolDetailsViewModelDelegate: SchoolDetailsViewModelDelegate {

    private(set) var sectionsUpdatesCounter = 0

    func schoolDetailsViewModelDidUpdateSections(_ viewModel: SchoolDetailsViewModel) {
        sectionsUpdatesCounter += 1
    }
}

