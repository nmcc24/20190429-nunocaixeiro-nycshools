//
//  MockSchoolsListViewModelDelegate.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest
@testable import NYC_Schools

class MockSchoolsListViewModelDelegate: SchoolsListViewModelDelegate {

    private(set) var startLoadingStatusCounter = 0
    private(set) var stopLoadingStatusCounter = 0
    private(set) var schoolsSuccessfulFetchCounter = 0
    private(set) var schoolsFailedFetchCounter = 0
    private(set) var schoolsFailedFetchErrorMessages = [String]()

    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didUpdateLoadingStatus isLoading: Bool) {
        if isLoading {
            startLoadingStatusCounter += 1
        } else {
            stopLoadingStatusCounter += 1
        }
    }

    func schoolsListViewModelDidFetchSchools(_ viewModel: SchoolsListViewModel) {
        schoolsSuccessfulFetchCounter += 1
    }

    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didFailFetchingWith errorMessage: String) {
        schoolsFailedFetchCounter += 1
        schoolsFailedFetchErrorMessages.append(errorMessage)
    }
}
