//
//  MockSatResultsService.swift
//  NYC SchoolsTests
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation
@testable import NYC_Schools

class MockSatResultsService: SatResultsServiceType {
    var endpointComponents = URLComponents()
    var shouldFail = false
    let successJSON: String

    init(successJSON: String) {
        self.successJSON = successJSON
    }

    func fetchResultForSchool(withId id: String, completion: @escaping (ServiceResult<SatResult?>) -> ()) {
        let result: ServiceResult<SatResult?>

        if shouldFail {
            result = .error(ResponseError.undefined("Test Response Error"))
        } else {
            let data = successJSON.data(using: .utf8)!
            let satResult: SatResult?

            if let resultsArray = try? JSONDecoder().decode([SatResult].self, from: data) {
                satResult = resultsArray.first
            } else {
                satResult = nil
            }

            result = .success(satResult)
        }

        completion(result)
    }
}

