//
//  NYC_SchoolsUITests.swift
//  NYC SchoolsUITests
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import XCTest

class NYC_SchoolsUITests: XCTestCase {

    override func setUp() {
        super.setUp()

        continueAfterFailure = false
        XCUIApplication().launch()
    }


    func testHappyPath() {
        let app = XCUIApplication()

        // School list screen must be displayed
        XCTAssertTrue(app.navigationBars[Constants.schoolsListBarTitle].exists)
        let schoolsListScreen = app.otherElements["SchoolsListScreen"].firstMatch
        XCTAssertTrue(schoolsListScreen.exists)

        // Wait until loading spinner disappears
        let spinner = schoolsListScreen.activityIndicators.firstMatch
        wait(for: spinner, toBeVisible: false)

        // Second cell should match "Liberation Diplome Plus High School" school
        let cell = schoolsListScreen.tables.cells.element(boundBy: 1)
        XCTAssertTrue(cell.staticTexts[Constants.schoolName].exists)

        // Tapping the cell should displays the school details
        cell.tap()
        XCTAssertTrue(app.navigationBars[Constants.schoolName].exists)
        let schoolDetailScreen = app.otherElements["SchoolDetailsScreen"].firstMatch
        XCTAssertTrue(schoolDetailScreen.exists)
        let schoolsDetailsTable = schoolDetailScreen.tables.element

        // All the details should be valid
        XCTAssertTrue(schoolsDetailsTable.otherElements[Constants.generalInformationSection].exists)
        let overviewCell = schoolsDetailsTable.cells["SchoolDetailCell-0-0"].firstMatch
        XCTAssertTrue(overviewCell.exists)
        let studentsCell = schoolsDetailsTable.cells["SchoolDetailCell-0-1"].firstMatch
        XCTAssertTrue(studentsCell.staticTexts[Constants.numberOfStudentsKey].exists)
        XCTAssertTrue(studentsCell.staticTexts[Constants.numberOfStudentsValue].exists)

        wait(for: schoolsDetailsTable.otherElements[Constants.satResultsSection], toBeVisible: true)
        let readingScoreCell = schoolsDetailsTable.cells["SchoolDetailCell-1-0"].firstMatch
        XCTAssertTrue(readingScoreCell.staticTexts[Constants.readingScoreKey].exists)
        XCTAssertTrue(readingScoreCell.staticTexts[Constants.readingScoreValue].exists)
        let mathScoreCell = schoolsDetailsTable.cells["SchoolDetailCell-1-1"].firstMatch
        XCTAssertTrue(mathScoreCell.staticTexts[Constants.mathScoreKey].exists)
        XCTAssertTrue(mathScoreCell.staticTexts[Constants.mathScoreValue].exists)
        let writingScoreCell = schoolsDetailsTable.cells["SchoolDetailCell-1-2"].firstMatch
        XCTAssertTrue(writingScoreCell.staticTexts[Constants.writingScoreKey].exists)
        XCTAssertTrue(writingScoreCell.staticTexts[Constants.writingScoreValue].exists)

        schoolsDetailsTable.swipeUp()
        XCTAssertTrue(schoolsDetailsTable.otherElements[Constants.addressSection].exists)
        let addressCell = schoolsDetailsTable.cells["SchoolDetailCell-2-0"].firstMatch
        XCTAssertTrue(addressCell.staticTexts[Constants.addressItem].exists)

        XCTAssertTrue(schoolsDetailsTable.otherElements[Constants.contactsSection].exists)
        let websiteCell = schoolsDetailsTable.cells["SchoolDetailCell-3-0"].firstMatch
        XCTAssertTrue(websiteCell.staticTexts[Constants.websiteKey].exists)
        XCTAssertTrue(websiteCell.staticTexts[Constants.websiteValue].exists)
        let phoneCell = schoolsDetailsTable.cells["SchoolDetailCell-3-1"].firstMatch
        XCTAssertTrue(phoneCell.staticTexts[Constants.phoneKey].exists)
        XCTAssertTrue(phoneCell.staticTexts[Constants.phoneValue].exists)
    }
}

private extension NYC_SchoolsUITests {
    enum Constants {
        static let schoolsListBarTitle = "NYC Schools"
        static let schoolName = "Liberation Diploma Plus High School"

        static let generalInformationSection = "GENERAL INFORMATION"
        static let numberOfStudentsKey = "Number of Students"
        static let numberOfStudentsValue = "206"

        static let satResultsSection = "SAT RESULTS"
        static let readingScoreKey = "Reading Average Score"
        static let readingScoreValue = "411"
        static let mathScoreKey = "Math Average Score"
        static let mathScoreValue = "369"
        static let writingScoreKey = "Writing Average Score"
        static let writingScoreValue = "373"

        static let addressSection = "ADDRESS"
        static let addressItem = "2865 West 19th Street\nBrooklyn\nNY 11224"

        static let contactsSection = "CONTACTS"
        static let websiteKey = "Website"
        static let websiteValue = "schools.nyc.gov/schoolportals/21/K728"
        static let phoneKey = "Phone Number"
        static let phoneValue = "718-946-6812"
    }

    func wait(for element: XCUIElement, toBeVisible: Bool) {
        let predicate = NSPredicate(format: "exists == \(toBeVisible)")
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: element)
        wait(for: [expectation], timeout: 5)
    }
}
