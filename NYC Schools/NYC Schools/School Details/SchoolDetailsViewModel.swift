//
//  SchoolDetailsViewModel.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

protocol SchoolDetailsViewModelDelegate: class {
    func schoolDetailsViewModelDidUpdateSections(_ viewModel: SchoolDetailsViewModel)
}

class SchoolDetailsViewModel {

    private enum Constants {
        static let generalInfoSection = "General Information"
        static let addressSection = "Address"
        static let contactsSection = "Contacts"
        static let satResultsSection = "SAT Results"

        static let studentsCountItemKey = "Number of Students"
        static let websiteItemKey = "Website"
        static let phoneItemKey = "Phone Number"
        static let satReadingItemKey = "Reading Average Score"
        static let satMathItemKey = "Math Average Score"
        static let satWritingItemKey = "Writing Average Score"
        static let noSatResultsItemKey = "No results available"
    }

    // MARK: Public variables
    weak var delegate: SchoolDetailsViewModelDelegate?

    var title: String {
        return school.name
    }

    private(set) lazy var sections: [Section] = {
        return [generalInfoSection, addressSection, contactsSection]
    }()

    // MARK: Private variables
    private let service: SatResultsServiceType
    private let school: School

    // MARK: Initialisation
    init(school: School, service: SatResultsServiceType = SatResultsService()) {
        self.school = school
        self.service = service
    }

    // MARK: SAT Results Fetching
    func fetchSatResult() {
        service.fetchResultForSchool(withId: school.id) { [weak self] result in

            let satResult: SatResult?

            // Gracefully handling error scenarios by logging them in stdout and displaying same unavailable results entry as when there are no results for a certain school
            switch(result) {
            case .error(let error):
                print("SAT result fetching did fail with error \(error)")
                satResult = nil
            case .success(let result):
                print("Did fetch SAT result: \(String(describing: result))")
                satResult = result
            }

            self?.createSatResultsSection(from: satResult)
        }
    }

    private func createSatResultsSection(from result: SatResult?) {
        let section: Section
        if let satResult = result {
            section = satResultsSection(from: satResult)
        } else {
            section = .satResults([.text(Constants.noSatResultsItemKey)])
        }

        sections.insert(section, at: 1)
        delegate?.schoolDetailsViewModelDidUpdateSections(self)
    }
}

// MARK: Cells View Models
extension SchoolDetailsViewModel {
    enum Section: Equatable {
        case generalInfo([Item])
        case satResults([Item])
        case address([Item])
        case contacts([Item])

        var title: String {
            switch self {
            case .generalInfo:
                return Constants.generalInfoSection
            case .satResults:
                return Constants.satResultsSection
            case .address:
                return Constants.addressSection
            case .contacts:
                return Constants.contactsSection
            }
        }
    }

    enum Item: Equatable {
        case text(String)
        case detail(key: String, value: String)
    }

    private var generalInfoSection: Section {
        let overview = Item.text(school.overview)
        let students = Item.detail(key: Constants.studentsCountItemKey, value: String(school.studentsCount))
        return .generalInfo([overview, students])
    }

    private var addressSection: Section {
        let overview = Item.text(school.fullAddress)
        return .address([overview])
    }

    private var contactsSection: Section {
        let website = Item.detail(key: Constants.websiteItemKey, value: String(school.website))
        let phone = Item.detail(key: Constants.phoneItemKey, value: school.phone)
        return .contacts([website, phone])
    }

    private func satResultsSection(from result: SatResult) -> Section {
        let reading = Item.detail(key: Constants.satReadingItemKey, value: String(result.averageReadingScore))
        let math = Item.detail(key: Constants.satMathItemKey, value: String(result.averageMathScore))
        let writing = Item.detail(key: Constants.satWritingItemKey, value: String(result.averageWritingScore))
        return .satResults([reading, math, writing])
    }
}

