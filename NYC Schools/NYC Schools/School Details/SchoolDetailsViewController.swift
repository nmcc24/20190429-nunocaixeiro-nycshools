//
//  SchoolDetailsViewController.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {

    private enum Constants {
        static let textCellIdentifier = "TextCell"
        static let detailCellIdentifier = "DetailCell"
        static let viewAccessibilityIdentifier = "SchoolDetailsScreen"
        static let cellAccessibilityIdenitifer = "SchoolDetailCell"
    }

    // MARK: IBOutlets
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
        }
    }

    // MARK: Public variables
    var viewModel: SchoolDetailsViewModel! {
        didSet {
            viewModel.delegate = self
        }
    }

    // MARK: View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title
        navigationItem.largeTitleDisplayMode = .never
        view.accessibilityIdentifier = Constants.viewAccessibilityIdentifier

        viewModel.fetchSatResult()
    }
}

extension SchoolDetailsViewController: SchoolDetailsViewModelDelegate {
    func schoolDetailsViewModelDidUpdateSections(_ viewModel: SchoolDetailsViewModel) {
        tableView.reloadData()
    }
}

extension SchoolDetailsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = viewModel.sections[section]

        switch section {
        case .generalInfo(let items), .satResults(let items), .address(let items), .contacts(let items):
            return items.count
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sections[section].title
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: SchoolDetailsViewModel.Item

        switch viewModel.sections[indexPath.section] {
            case .generalInfo(let items), .satResults(let items), .address(let items), .contacts(let items):
            item = items[indexPath.row]
        }

        let cell: UITableViewCell

        switch item {
        case .text(let text):
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.textCellIdentifier, for: indexPath)
            cell.textLabel?.text = text
        case .detail(let key, let value):
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.detailCellIdentifier, for: indexPath)
            cell.textLabel?.text = key
            cell.detailTextLabel?.text = value
        }

        cell.accessibilityIdentifier = "\(Constants.cellAccessibilityIdenitifer)-\(indexPath.section)-\(indexPath.row)"

        return cell
    }
}
