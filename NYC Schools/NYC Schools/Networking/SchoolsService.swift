//
//  SchoolsService.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

protocol SchoolsServiceType: NYCService {
    func fetch(completion: @escaping (ServiceResult<[School]>) -> ())
}

class SchoolsService: SchoolsServiceType {
    private struct Constants {
        static let resourcePath = "/s3k6-pzi2.json"
    }

    let endpointComponents: URLComponents
    private let dataDownloader: DataDownloader

    init(endpointComponents: URLComponents, dataDownloader: DataDownloader = DataDownloader()) {
        self.endpointComponents = endpointComponents
        self.dataDownloader = dataDownloader
    }

    convenience init(dataDownloader: DataDownloader = DataDownloader()) {
        let components = ServiceConstants.urlComponents(withPath: Constants.resourcePath)
        self.init(endpointComponents: components, dataDownloader: dataDownloader)
    }

    func fetch(completion: @escaping (ServiceResult<[School]>) -> ()) {
        guard let url = endpointComponents.url else {
            return completion(.error(ServiceError.invalidURL))
        }

        var request = URLRequest(url: url)
        request.setHttpHeader(with: requestHeaderFields)

        dataDownloader.dataTask(with: request) { result in
            DispatchQueue.main.async {
                switch(result) {
                case .error(let error): completion(.error(error))
                case .success(let data):
                    do {
                        let schools = try JSONDecoder().decode([School].self, from: data)
                        completion(.success(schools))
                    } catch {
                        completion(.error(ServiceError.failedJsonDecoding))
                    }
                }
            }
        }.resume()
    }
}

