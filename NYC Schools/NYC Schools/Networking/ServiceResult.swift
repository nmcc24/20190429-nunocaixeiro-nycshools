//
//  ServiceResult.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

enum ServiceResult<T> {
    case success(T)
    case error(Error)
}

enum ServiceError: Error, Equatable {
    case invalidURL
    case failedJsonDecoding
}

enum ResponseError: Error  {
    case noResponseData
    case invalidResponseData
    case undefined(String)
}

extension ResponseError: Equatable {
    static func ==(lhs: ResponseError, rhs: ResponseError) -> Bool {
        switch (lhs, rhs) {
        case (.noResponseData, .noResponseData),
             (.invalidResponseData, .invalidResponseData):
            return true

        case (let .undefined(string1), let .undefined(string2)):
            return string1 == string2

        default:
            return false
        }
    }
}

