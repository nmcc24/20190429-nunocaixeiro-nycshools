//
//  SatResultsService.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

protocol SatResultsServiceType: NYCService {
    func fetchResultForSchool(withId id: String, completion: @escaping (ServiceResult<SatResult?>) -> ())
}

class SatResultsService: SatResultsServiceType {
    private struct Constants {
        static let resourcePath = "/f9bf-2cp4.json"
        static let schoolIdQueryKey = "dbn"
    }

    let endpointComponents: URLComponents
    private let dataDownloader: DataDownloader

    init(endpointComponents: URLComponents, dataDownloader: DataDownloader = DataDownloader()) {
        self.endpointComponents = endpointComponents
        self.dataDownloader = dataDownloader
    }

    convenience init(dataDownloader: DataDownloader = DataDownloader()) {
        let components = ServiceConstants.urlComponents(withPath: Constants.resourcePath)
        self.init(endpointComponents: components, dataDownloader: dataDownloader)
    }

    func fetchResultForSchool(withId id: String, completion: @escaping (ServiceResult<SatResult?>) -> ()) {
        var localEndpointComponents = endpointComponents
        localEndpointComponents.query = "\(Constants.schoolIdQueryKey)=\(id)"

        guard let url = localEndpointComponents.url else {
            return completion(.error(ServiceError.invalidURL))
        }

        var request = URLRequest(url: url)
        request.setHttpHeader(with: requestHeaderFields)

        dataDownloader.dataTask(with: request) { result in
            DispatchQueue.main.async {
                switch(result) {
                case .error(let error): completion(.error(error))
                case .success(let data):

                    // Some responses contain SatResult objects with invalid values ("s" instead of an actual value for all fields). In that event, it is considered that there is no result.
                    if let resultsArray = try? JSONDecoder().decode([SatResult].self, from: data) {
                        completion(.success(resultsArray.first))
                    } else {
                        completion(.success(nil))
                    }
                }
            }
        }.resume()
    }
}

