//
//  DataDownloader.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

class DataDownloader {

    let session: URLSession

    init(session: URLSession = .shared) {
        self.session = session
    }

    /// Creates a task that retrieves the contents of a URL based on the specified URL request object, and calls a handler upon completion.
    ///
    /// - Parameters:
    ///   - request: A URL request object that provides the URL, cache policy, request type, body data or body stream, and so on.
    ///   - completion: The completion handler to call when the load request is complete. This handler is executed on a background queue.

    /// - Returns: The new session data task.
    func dataTask(with request: URLRequest, completion: @escaping (ServiceResult<Data>) -> ()) -> URLSessionDataTask {
        return session.dataTask(with: request) { (data, response, error) in
            if let responseError = error {
                completion(.error(ResponseError.undefined(responseError.localizedDescription)))
                return
            }

            guard let responseData = data else {
                completion(.error(ResponseError.noResponseData))
                return
            }

            // Ideally one would look at check if the response is an HTTPURLResponse and rely on status codes to understand the status of the request.
            // However, since Socrata has no API documentation about this and their SDKs only looks at response JSON for both success and failure scenarios, I'm replicating the same behaviour.
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData)

                if jsonResult is [[String: Any]] {
                    completion(.success(responseData))

                } else if let dict = jsonResult as? [String: Any] {
                    guard dict["error"] == nil else {
                        let message = dict["message"] as? String ?? ""
                        completion(.error(ResponseError.undefined(message)))
                        return
                    }

                    completion(.success(responseData))
                }
            } catch {
                completion(.error(ResponseError.invalidResponseData))
            }
        }
    }
}
