//
//  NYCService.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

protocol NYCService {
    var endpointComponents: URLComponents { get }

    var requestHeaderFields: [String: String] { get }
}

extension NYCService {
    var requestHeaderFields: [String: String] {
        return ["X-App-Token": "cSXeLBmN3WkTgRKwdanapqYha",
               "Accept": "application/json"]
    }
}

enum ServiceConstants {
    private static let scheme = "https"
    private static let host = "data.cityofnewyork.us"
    private static let basePath = "/resource"

    static func urlComponents(withPath path: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host

        // Normalises the received path by removing initial / if it exists
        let normalisedPath = path.first == "/" ? String(path.dropFirst()) : path
        components.path = "\(basePath)/\(normalisedPath)"

        return components
    }
}
