//
//  UIViewController+ClassIdentifiable.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

protocol ClassIdentifiable: class {
    static func identifierFromClassName() -> String
}

extension ClassIdentifiable {
    static func identifierFromClassName() -> String {
        let classString = NSStringFromClass(self)

        guard let identifier = classString.components(separatedBy: ".").last else {
            fatalError("Problem generating reuse identifier for class \(classString)")
        }

        return identifier
    }
}

extension UIViewController: ClassIdentifiable {
    public static var segueIdentifier: String {
        return "show" + identifierFromClassName()
    }
}
