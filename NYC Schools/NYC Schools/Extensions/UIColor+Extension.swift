//
//  UIColor+Extension.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

extension UIColor {
    class var chaseBlue: UIColor {
        return color(named: "Chase Blue")
    }

    class var chaseGrey: UIColor {
        return color(named: "Chase Grey")
    }

    private class func color(named colorName: String) -> UIColor {
        guard let color = UIColor(named: colorName) else {
            preconditionFailure("Asset catalog should contain color named \(colorName).")
        }

        return color
    }
}
