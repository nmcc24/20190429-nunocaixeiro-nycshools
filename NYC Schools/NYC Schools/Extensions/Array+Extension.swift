//
//  Array+Extension.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

extension Array {
    public subscript(safe index: Int) -> Element? {
        guard index >= 0, index < endIndex else {
            return nil
        }

        return self[index]
    }
}
