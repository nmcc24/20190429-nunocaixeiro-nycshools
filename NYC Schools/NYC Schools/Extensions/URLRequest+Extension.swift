//
//  URLRequest+Extension.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

extension URLRequest {
    mutating func setHttpHeader(with fields: [String: String]) {
        for (field, value) in fields {
            setValue(value, forHTTPHeaderField: field)
        }
    }
}
