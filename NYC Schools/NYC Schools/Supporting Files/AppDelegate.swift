//
//  AppDelegate.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppearanceConfigurator.configureApplicationStyling()
        return true
    }
}

