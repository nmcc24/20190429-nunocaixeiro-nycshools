//
//  AppearanceConfigurator.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation
import UIKit

protocol AppearanceConfigurable {
    static func configureAppearance()
}

final class AppearanceConfigurator {

    static func configureApplicationStyling() {
        if let window = UIApplication.shared.delegate?.window as? UIWindow {
            window.backgroundColor = .groupTableViewBackground
        }

        NavigationController.configureAppearance()
    }
}
