//
//  NavigationController.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension NavigationController: AppearanceConfigurable {
    static func configureAppearance() {
        styleNavigationBar()
    }

    private static func styleNavigationBar() {
        let appearance = UINavigationBar.appearance(whenContainedInInstancesOf: [NavigationController.self])
        appearance.barTintColor = .chaseBlue
        appearance.tintColor = .white
        appearance.isTranslucent = false

        let titleAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.titleTextAttributes = titleAttributes
        appearance.largeTitleTextAttributes = titleAttributes
    }
}
