//
//  SchoolsListViewController.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import UIKit

class SchoolsListViewController: UIViewController {

    private enum Constants {
        static let cellIdentifier = "SchoolCell"
        static let viewAccessibilityIdentifier = "SchoolsListScreen"
    }

    // MARK: IBOutlets
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }

    @IBOutlet private weak var loadingSpinner: UIActivityIndicatorView!

    @IBOutlet private weak var messageLabel: UILabel!

    // MARK: Private variables
    private lazy var viewModel: SchoolsListViewModel = {
        let viewModel = SchoolsListViewModel()
        viewModel.delegate = self
        return viewModel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = SchoolsListViewModel.Constants.title
        navigationItem.largeTitleDisplayMode = .always
        view.accessibilityIdentifier = Constants.viewAccessibilityIdentifier

        prepareUI()
        viewModel.fetchSchools()
    }

    // MARK: UI Configuration
    private func prepareUI() {
        tableView.isHidden = true
        loadingSpinner.isHidden = true
        messageLabel.isHidden = true
    }

    private func updateSpinner(_ animating: Bool) {
        if animating {
            loadingSpinner.isHidden = false
            loadingSpinner.startAnimating()
        } else {
            loadingSpinner.isHidden = true
            loadingSpinner.stopAnimating()
        }
    }

    private func updateUITo(showResults: Bool, errorMessage: String?) {
        tableView.isHidden = !showResults
        messageLabel.text = errorMessage
        messageLabel.isHidden = showResults
    }

    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == SchoolDetailsViewController.segueIdentifier,
            let destination = segue.destination as? SchoolDetailsViewController,
            let school = sender as? School else {
                preconditionFailure("Received unexpected segue")
        }

        destination.viewModel = SchoolDetailsViewModel(school: school)
    }
}

// MARK: UITableViewDataSource
extension SchoolsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let school = viewModel.schools[safe: indexPath.row] else {
            preconditionFailure("A school is expected at this stage.")
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath)
        cell.textLabel?.text = school.name
        return cell
    }
}

// MARK: UITableViewDelegate
extension SchoolsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedSchool = viewModel.schools[safe: indexPath.row] else {
            preconditionFailure("A school is expected at this stage.")
        }
        performSegue(withIdentifier: SchoolDetailsViewController.segueIdentifier, sender: selectedSchool)
    }
}

// MARK: SchoolsListViewModelDelegate
extension SchoolsListViewController: SchoolsListViewModelDelegate {

    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didUpdateLoadingStatus isLoading: Bool) {
        updateSpinner(isLoading)
    }

    func schoolsListViewModelDidFetchSchools(_ viewModel: SchoolsListViewModel) {
        updateUITo(showResults: true, errorMessage: nil)
        tableView.reloadData()
    }

    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didFailFetchingWith errorMessage: String) {
        updateUITo(showResults: false, errorMessage: errorMessage)
    }
}

