//
//  SchoolsListViewModel.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 26/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

protocol SchoolsListViewModelDelegate: class {
    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didUpdateLoadingStatus isLoading: Bool)
    func schoolsListViewModelDidFetchSchools(_ viewModel: SchoolsListViewModel)
    func schoolsListViewModel(_ viewModel: SchoolsListViewModel, didFailFetchingWith errorMessage: String)
}

class SchoolsListViewModel {

    enum Constants {
        static let title = "NYC Schools"
        static let errorMessage = "There was a problem loading the list of schools."
    }

    weak var delegate: SchoolsListViewModelDelegate?

    // MARK: Private variables
    private let service: SchoolsServiceType

    // MARK: Initialisation
    init(service: SchoolsServiceType = SchoolsService()) {
        self.service = service
    }

    // MARK: Schoools Fetching
    private var isLoading = false {
        didSet {
            delegate?.schoolsListViewModel(self, didUpdateLoadingStatus: isLoading)
        }
    }
    private(set) var schools = [School]() {
        didSet {
            delegate?.schoolsListViewModelDidFetchSchools(self)
        }
    }

    func fetchSchools() {
        isLoading = true

        service.fetch { [weak self] result in
            self?.isLoading = false

            switch(result) {
            case .error(let error): self?.didFailSchoolsFetching(with: error)
            case .success(let schools):
                self?.didFetchSchools(schools)
            }
        }
    }

    private func didFailSchoolsFetching(with error: Error) {
        print("Schools fetching did fail with error \(error)")
        delegate?.schoolsListViewModel(self, didFailFetchingWith: Constants.errorMessage)
    }

    private func didFetchSchools(_ schools: [School]) {
        self.schools = schools
    }
}

