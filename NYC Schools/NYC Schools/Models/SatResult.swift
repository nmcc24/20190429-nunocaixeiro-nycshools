//
//  SatResult.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 25/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

struct SatResult: Codable, Equatable {
    let studentsCount: Int
    let averageReadingScore: Int
    let averageMathScore: Int
    let averageWritingScore: Int

    private enum CodingKeys: String, CodingKey {
        case studentsCount = "num_of_sat_test_takers"
        case averageReadingScore = "sat_critical_reading_avg_score"
        case averageMathScore = "sat_math_avg_score"
        case averageWritingScore = "sat_writing_avg_score"
    }
}

// MARK: Decodable
extension SatResult {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        let studentCountString = try values.decode(String.self, forKey: .studentsCount)
        let readingScoreString = try values.decode(String.self, forKey: .averageReadingScore)
        let mathScoreString = try values.decode(String.self, forKey: .averageMathScore)
        let writingScoreString = try values.decode(String.self, forKey: .averageWritingScore)

        guard let studentsCount = Int(studentCountString),
            let readingScore = Int(readingScoreString),
            let mathScore = Int(mathScoreString),
            let writingScore = Int(writingScoreString) else {
            throw ServiceError.failedJsonDecoding
        }

        self.studentsCount = studentsCount
        averageReadingScore = readingScore
        averageMathScore = mathScore
        averageWritingScore = writingScore
    }
}

// MARK: Encodable
extension SatResult {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(String(studentsCount), forKey: .studentsCount)
        try container.encode(String(averageReadingScore), forKey: .averageReadingScore)
        try container.encode(String(averageMathScore), forKey: .averageMathScore)
        try container.encode(String(averageWritingScore), forKey: .averageWritingScore)
    }
}
