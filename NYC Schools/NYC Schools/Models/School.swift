//
//  School.swift
//  NYC Schools
//
//  Created by Nuno Caixeiro on 24/04/2019.
//  Copyright © 2019 Nuno Caixeiro. All rights reserved.
//

import Foundation

struct School: Codable, Equatable {
    let id: String
    let name: String
    let overview: String
    let studentsCount: Int
    let website: String
    let phone: String
    let primaryAddressLine: String
    let city: String
    let stateCode: String
    let zipCode: String

    private enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case overview = "overview_paragraph"
        case studentsCount = "total_students"
        case website
        case phone = "phone_number"
        case primaryAddressLine = "primary_address_line_1"
        case city
        case stateCode = "state_code"
        case zipCode = "zip"
    }
}

extension School {
    var fullAddress: String {
        return "\(primaryAddressLine)\n\(city)\n\(stateCode) \(zipCode)"
    }
}

// MARK: Decodable
extension School {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        overview = try values.decode(String.self, forKey: .overview)

        let studentCountString = try values.decode(String.self, forKey: .studentsCount)
        guard let count = Int(studentCountString) else {
            throw ServiceError.failedJsonDecoding
        }
        studentsCount = count

        website = try values.decode(String.self, forKey: .website)
        phone = try values.decode(String.self, forKey: .phone)
        primaryAddressLine = try values.decode(String.self, forKey: .primaryAddressLine)
        city = try values.decode(String.self, forKey: .city)
        stateCode = try values.decode(String.self, forKey: .stateCode)
        zipCode = try values.decode(String.self, forKey: .zipCode)
    }
}

// MARK: Encodable
extension School {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(overview, forKey: .overview)
        try container.encode(String(studentsCount), forKey: .studentsCount)
        try container.encode(website, forKey: .website)
        try container.encode(phone, forKey: .phone)
        try container.encode(primaryAddressLine, forKey: .primaryAddressLine)
        try container.encode(city, forKey: .city)
        try container.encode(stateCode, forKey: .stateCode)
        try container.encode(zipCode, forKey: .zipCode)
    }
}
