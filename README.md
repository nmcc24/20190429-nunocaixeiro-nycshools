# NYC Schools

Thank you for reading this file and reviewing the code.

This document aims to provide some insight on how I approached this challenge, namely:

* Spec and API analysis
* Requirements definition
* Design and implementation
* Final result
* Future work


### Spec and API analysis
The proposed challenge is to list all New York City High Schools and then be able to view details about each one, including their SAT average scores. 

In order to achieve this, we need to obtain data from *NYC OpenData* `https://data.cityofnewyork.us`, namely from the following endpoints:

* *GET /resource/s3k6-pzi2.json* gives us access to the directory of Department of Education Schools in 2017. This allows us to retrieve a json file containing a list of school objects.
* *GET /resource/f9bf-2cp4.json* gives us access to the school results for New York City on the SAT taken during 2012. By appending a school `dbn` as a query parameter, it provides the result object for the respective school, if available. 

### Requirements definition

After analysing the provided spec and the APIs available I was empowered to define the scope of the project:

* **Fetch and display list of schools**: Obtaining the list of schools is essential to the challenge, allowing the user to view the name of each available one. Furthermore, more details will be displayed once he interacts with a particular school.

* **Fetch and display school details**: As per the spec, displaying the average SAT scores for a school is a requirement which is fullfilled when the user selects a certain school. To add up, other details such as overview and address are also displayed in order to provide more value to the user.

In order to keep the scope contained and reasonable, the following assumptions were made:
 
* Some SAT Result objects contain invalid values for all score fields  (eg. `"num_of_sat_test_takers": "s"`). Since there is no documentation about this and I would expect the API to only provide valid results, invalid results are discarded.
* If there was an error while obtaining an SAT result, I am gracefully handling it by following the approach as when there is an invalid or no result available. In other words `No results available` will be displayed on the SAT Results section.


### Design and implementation

In order to complete the technical challenge, the application was designed using Model - View - View Model (MVVM) architecture, relying on delegation pattern for communication from the View Model to the corresponding View Controller.
The View Model is responsible for providing the data required to populate the View Controller's views. Furthermore, it relies on services and corresponding network requests to obtain the necessary data and state required to make the application work as intended.

The application consists of two major areas: 

* **Schools list screen**, where the user can see the high schools available in New York.	
	* If there are schools available after loading, these will be displayed to the user

	<img src="ReadMe/ReadMe_Schools_List.png" width="250">

	* If an error occurred during the fetching process, a generic message will be displayed.

	<img src="ReadMe/ReadMe_Schools_List_Error.png" width="250">

* **School details screen** which is displayed once the a user taps on a particular school. This screen displays varied information about the selected school, including their SAT average scores if available.

<img src="ReadMe/ReadMe_School_Details_1.png" width="250"> <img src="ReadMe/ReadMe_School_Details_2.png" width="250">

Not least important, the entire system is subject to unit and UI testing in order to ensure its reliabilty and detect potential future regressions at an early stage. 

<img src="ReadMe/ReadMe_Testing_Code_Coverage.png" width="500">


## Instructions

The project is ready to be executed, with no preparation stage required. If by any change there is a problem related to the requests and App Token, please update `requestHeaderFields` in `NYCService.swift` property with the updated `X-App-Token`


## Future work

With the project in its current state, we are able to fullfil all the desired requirements. Furthermore, the whole code base has a very good test coverage at both unit and UI level.

Some tradeoffs had to me made in order not to overextend the time I spent with this project. Below is the set of tasks I would focus on if I could spend more time with this project: 

* Use pagination for fetching list of schools in smaller chunks;
* Provide filtering functionality to be able to easily search for a particular school;
* Adopt `UISplitViewController` in order to provide an improved iPad experience.